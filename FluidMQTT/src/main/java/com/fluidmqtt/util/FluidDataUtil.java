/**
 * 
 */
package com.fluidmqtt.util;

/**
 * @author mazhar
 *
 * Feb 18, 2020
 */
public class FluidDataUtil {
   public static final String DISPLAY_CLIENT = "UUI8~";
   public static final String ATOM_CLIENT = "UUA88s";
   public static final String PRIMARY_BROKER = "tcp://localhost:1883";
   
   public static final int NON_PERSISTENCE_QOS =0;
   public static final int PERSISTENCE_QOS = 1;
   public static final int PERSISTENCE_UNIQUE_QOS =2;
}
