/**
 * 
 */
package com.fluidmqtt.util;

/**
 * @author mazhar
 *
 * Feb 8, 2020
 */
public class FluidCommonMessages {
   public static final String INITIAL_DISPLAY_MESSAGE = "Welcome to Fluid. Fluid is a MQTT based Client Subscriber"
   		+ "real time communication System. if you are not connected please connect to start!";
   public static final String CONNECTED = "CONNECTED!!";
   public static final String NOT_CONNECTED = "NOT CONNECTED!!";
   
   public static final String COMMAND_FAN_ON = "Fo";
   public static final String COMMAND_TV_ON = "to";
   public static final String COMMAND_LIGHT_ON = "lo";
   public static final String COMMAND_FAN_OFF = "ff";
   public static final String COMMAND_TV_OFF = "tf";
   public static final String COMMAND_LIGHT_OFF = "lf";
   public static final String NO_COMMAND = "No command found";
}
