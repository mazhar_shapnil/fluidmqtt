/**
 * 
 */
package com.fluidmqtt.util;

import org.eclipse.paho.client.mqttv3.MqttTopic;
import org.eclipse.paho.client.mqttv3.internal.ClientComms;

/**
 * @author mazhar
 *
 * Feb 1, 2020
 */
public class FluidTopics  {
    
	public static final String TV_TOPICS = "tvos"; 
	public static final String FAN_TOPICS = "fanos";
	public static final String LIGHT_TOPICS = "lightos";
	
}
