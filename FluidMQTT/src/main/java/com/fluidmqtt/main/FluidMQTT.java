/**
 * 
 */
package com.fluidmqtt.main;

import com.fluidmqtt.connection.AtomClient;
import com.fluidmqtt.connection.FluidDisplayClient;
import com.fluidmqtt.util.FluidDataUtil;

/**
 * @author mazhar
 *
 * Feb 18, 2020
 */
public class FluidMQTT {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		FluidDisplayClient.makeClient(FluidDataUtil.PRIMARY_BROKER, FluidDataUtil.DISPLAY_CLIENT);
		AtomClient.makeConnection(FluidDataUtil.PRIMARY_BROKER, FluidDataUtil.ATOM_CLIENT);

	}

}
