/**
 * 
 */
package com.fluidmqtt.ui;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;

import com.fluidmqtt.connection.FluidDisplayClient;
import com.fluidmqtt.controller.FluidDisplayController;
import com.fluidmqtt.util.FluidCommonMessages;

import net.miginfocom.swing.MigLayout;

/**
 * @author mazhar
 *
 *         Feb 3, 2020
 */
public class FluidWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton clear;
	private JButton wiget;
	private JButton connection;
	private JToggleButton tv;
	private JToggleButton fan;
	private JToggleButton light;
	private ConnectionWindow conn = null;
	private JLabel statusLabel;
	private JTextArea display;

	public JPanel layer;
	private JButton logout;
	MqttClient client = FluidDisplayClient.getConnection();
	private FluidDisplayController fluidDisplay;

	public FluidWindow() {
		initUI();
		UpdateView();
		this.setTitle("Fluid Display");
		setIconImage(new javax.swing.ImageIcon(getClass().getResource("/icon.png")).getImage());
	}

	/**
	 * 
	 */
	private void initUI() {
		JPanel mainPanel = new JPanel(new BorderLayout());
		JPanel topPanel = new JPanel(new BorderLayout(10, 10));
		JPanel statusPanel = new JPanel();
		JPanel centerPanle = new JPanel(new CardLayout());
		JPanel header = new JPanel(new MigLayout());
		JPanel buttonPanel = new JPanel(new MigLayout("center"));
		logout = new JButton();
//toggle button make		
		tv = createToggoleButton("TV");
		fan = createToggoleButton("Fan");
		light = createToggoleButton("Light");
//button create		
		clear = createButton(clear, "Clear");
		clear.setPreferredSize(new Dimension(120, 35));
		clear.setBackground(Color.orange);

		wiget = createButton(wiget, "Wiget");
		wiget.setPreferredSize(new Dimension(140, 40));
		wiget.setBackground(Color.GREEN);

		connection = createButton(connection, "Connection");
		connection.setPreferredSize(new Dimension(140, 40));
		connection.setBackground(Color.blue);

//		add button to header
		header.add(connection, "cell 0 0");
		header.add(wiget, "pos 98%-pref 0.5al");

		topPanel.add(header, BorderLayout.NORTH);
		topPanel.add(getStatusBar(), BorderLayout.SOUTH);

		centerPanle.setBackground(Color.white);
		centerPanle.setBorder(BorderFactory.createLineBorder(Color.green, 2));

//panel size	

		mainPanel.setPreferredSize(new Dimension(600, 500));
		topPanel.setPreferredSize(new Dimension(600, 100));
		buttonPanel.setPreferredSize(new Dimension(600, 80));

		logout.setBackground(Color.white);
		logout.setBorderPainted(false);
		logout.setFocusable(false);
		logout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/logout.png")));
		logout.setOpaque(true);

		buttonPanel.add(clear);
		buttonPanel.add(fan);
		buttonPanel.add(tv);
		buttonPanel.add(light, "wrap");
		buttonPanel.add(logout, "pos 98%-pref 0.5al");

		buttonPanel.setAlignmentX(FlowLayout.RIGHT);
		centerPanle.add(createCenterPanel());
		mainPanel.add(topPanel, BorderLayout.NORTH);
		mainPanel.add(centerPanle, BorderLayout.CENTER);
		mainPanel.add(buttonPanel, BorderLayout.SOUTH);

//		add Action listener for button
		wiget.addActionListener(e -> openWiget());
		clear.addActionListener(e -> clearDisplay());
		connection.addActionListener(e -> createConnection());
		logout.addActionListener(e -> doLogout());
		tv.addActionListener(e -> fluidDisplay.actionTv());
		fan.addActionListener(e -> fluidDisplay.actionFan());
		light.addActionListener(e -> fluidDisplay.actionLight());
		this.add(mainPanel);
		this.setResizable(false);
		pack();

	}

	private void doLogout() {

		System.exit(0);
	}

	private void createConnection() {
		conn = ConnectionWindow.getConnectionWindow();
		if (client.isConnected()) {
			try {
				client.disconnect();
				UpdateView();
			} catch (MqttException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			conn.setVisible(true);
			conn.addWindowListener(new WindowListener() {

				@Override
				public void windowOpened(WindowEvent arg0) {
					UpdateView();

				}

				@Override
				public void windowIconified(WindowEvent arg0) {
					UpdateView();

				}

				@Override
				public void windowDeiconified(WindowEvent arg0) {
					UpdateView();

				}

				@Override
				public void windowDeactivated(WindowEvent arg0) {
					UpdateView();

				}

				@Override
				public void windowClosing(WindowEvent arg0) {
					UpdateView();

				}

				@Override
				public void windowClosed(WindowEvent arg0) {
					UpdateView();

				}

				@Override
				public void windowActivated(WindowEvent arg0) {
					UpdateView();

				}
			});
		}
	}

	/**
	 * @return
	 */
	private void clearDisplay() {
		display.setText("");
		fluidDisplay.clearDisplay();

	}

	/**
	 * @return
	 */
	private void openWiget() {
		WigetWindow wiget = WigetWindow.getWigets();
		if (wiget.isActive()) {
			return;
		} else {
			wiget.setVisible(true);
		}

	}

	private JButton createButton(JButton button, String name) {
		button = new JButton();
		button.setOpaque(true);
		button.setFocusable(false);
		button.setText(name);
		button.setFont(new Font("", Font.BOLD, 16));
		button.setForeground(Color.white);
		return button;
	}

	private JToggleButton createToggoleButton(String name) {
		JToggleButton btn = new JToggleButton();
		btn.setPreferredSize(new Dimension(80, 35));
		btn.setText(name);
		btn.setBackground(Color.white);
		btn.setOpaque(true);
		btn.setFocusable(false);
		return btn;
	}


	private JPanel getStatusBar() {
		JPanel status = new JPanel();
		layer = new JPanel(new FlowLayout());
		statusLabel = new JLabel();
		status.setPreferredSize(new Dimension(600, 50));
		layer.setPreferredSize(new Dimension(590, 40));
		layer.setBackground(Color.red);
		statusLabel.setForeground(Color.white);
		statusLabel.setFont(new Font("Helvetica", Font.BOLD, 24));
		statusLabel.setText(FluidCommonMessages.NOT_CONNECTED);
		statusLabel.setOpaque(true);
		statusLabel.setBackground(Color.red);
		layer.add(statusLabel);
		status.add(layer);
		return status;
	}

	private JPanel createCenterPanel() {
		JPanel layer = new JPanel(new CardLayout());
		display = new JTextArea();
		display.setFont(new Font("Helvetica", Font.PLAIN, 14));
		display.setLineWrap(true);
		display.setEditable(false);
		display.setText(FluidCommonMessages.INITIAL_DISPLAY_MESSAGE);
		layer.add(display);
		return layer;

	}

	private void UpdateView() {
		if (client.isConnected()) {
			fluidDisplay = new FluidDisplayController(this);
			statusLabel.setText(FluidCommonMessages.CONNECTED);
			layer.setBackground(Color.green);
			statusLabel.setBackground(Color.green);
			connection.setText("Disconnect");
			connection.setBackground(Color.red);
		} else {
			statusLabel.setText(FluidCommonMessages.NOT_CONNECTED);
			layer.setBackground(Color.red);
			statusLabel.setBackground(Color.red);
			connection.setText("Connect");
			connection.setBackground(Color.green);
		}

		repaint();
		revalidate();
	}

	public JTextArea getDisplay() {
		return display;
	}

	public void setDisplay(JTextArea display) {
		this.display = display;
	}
	public JToggleButton getTv() {
		return tv;
	}

	public void setTv(JToggleButton tv) {
		this.tv = tv;
	}

	public JToggleButton getFan() {
		return fan;
	}

	public void setFan(JToggleButton fan) {
		this.fan = fan;
	}

	public JToggleButton getLight() {
		return light;
	}

	public void setLight(JToggleButton light) {
		this.light = light;
	}


}
