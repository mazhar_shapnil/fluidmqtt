/*
 * 
 */
package com.fluidmqtt.controller;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;

import com.fluidmqtt.connection.FluidDisplayClient;
import com.fluidmqtt.ui.FluidWindow;
import com.fluidmqtt.util.FluidDataUtil;
import com.fluidmqtt.util.FluidCommonMessages;
import com.fluidmqtt.util.FluidTopics;
import com.mchange.v2.lang.StringUtils;

/*
 * @author mazhar
 *
 * Feb 15, 2020
 */
public class FluidDisplayController implements MqttCallback {
	private FluidWindow window;
	MqttClient client = FluidDisplayClient.getConnection();
	private StringBuffer clientResponse = new StringBuffer();

	public FluidDisplayController(FluidWindow window) {
		this.window = window;
		try {
			if (!client.isConnected()) {
				client.connect();
			}

			client.subscribe(FluidTopics.FAN_TOPICS);
			client.subscribe(FluidTopics.TV_TOPICS);
			client.subscribe(FluidTopics.LIGHT_TOPICS);
		} catch (MqttException e1) {
			e1.printStackTrace();
		}
		client.setCallback(this);

	}

	@Override
	public void connectionLost(Throwable cause) {
		System.out.println("connection lost from Display controller" + cause);

	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		clientResponse.append("message from " + topic + " : " + message + "\n");
		window.getDisplay().setText(clientResponse.toString());
		updateToggoleButtonStatus(message.toString());

	}

	/**
	 * @param string
	 */
	private void updateToggoleButtonStatus(String string) {
		if (string.equalsIgnoreCase(FluidCommonMessages.COMMAND_TV_ON)) {
			window.getTv().setSelected(true);
		} else if (string.equalsIgnoreCase(FluidCommonMessages.COMMAND_FAN_ON)) {
			window.getFan().setSelected(true);
		} else if (string.equalsIgnoreCase(FluidCommonMessages.COMMAND_LIGHT_ON)) {
			window.getLight().setSelected(true);
		}

	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		System.out.println("delivery  from display controller" + token);

	}

	public void clearDisplay() {
		clientResponse.delete(0, clientResponse.length());
	}

	public void actionTv() {
		try {
			if (window.getTv().isSelected()) {
				client.publish(FluidTopics.TV_TOPICS, sendCommand(FluidCommonMessages.COMMAND_TV_ON));
			} else {
				client.publish(FluidTopics.TV_TOPICS, sendCommand(FluidCommonMessages.COMMAND_TV_OFF));
			}

		} catch (MqttPersistenceException e) {
			e.printStackTrace();
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}

	public void actionFan() {
		try {
			if (window.getFan().isSelected()) {
				client.publish(FluidTopics.FAN_TOPICS, sendCommand(FluidCommonMessages.COMMAND_FAN_ON));
			} else {
				client.publish(FluidTopics.FAN_TOPICS, sendCommand(FluidCommonMessages.COMMAND_FAN_OFF));
			}

		} catch (MqttPersistenceException e) {
			e.printStackTrace();
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}

	public void actionLight() {
		try {
			if (window.getLight().isSelected()) {
				client.publish(FluidTopics.LIGHT_TOPICS, sendCommand(FluidCommonMessages.COMMAND_LIGHT_ON));
			} else {
				client.publish(FluidTopics.LIGHT_TOPICS, sendCommand(FluidCommonMessages.COMMAND_LIGHT_OFF));
			}

		} catch (MqttPersistenceException e) {
			e.printStackTrace();
		} catch (MqttException e) {
			e.printStackTrace();
		}

	}

	private MqttMessage sendCommand(String message) {
		if (!StringUtils.nonEmptyString(message)) {
			message = FluidCommonMessages.NO_COMMAND;
		}
		MqttMessage mqMessage = new MqttMessage(message.getBytes());
		mqMessage.setQos(FluidDataUtil.PERSISTENCE_QOS);
		return mqMessage;
	}

}
