/*
 *Client and Fluid Display is different window so they need to separate
 * 
 */
package com.fluidmqtt.controller;

import java.awt.Color;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import com.fluidmqtt.connection.AtomClient;
import com.fluidmqtt.ui.ClientView;
import com.fluidmqtt.util.FluidCommonMessages;
import com.fluidmqtt.util.FluidDataUtil;
import com.fluidmqtt.util.FluidTopics;
import com.mchange.v2.lang.StringUtils;

/**
 * @author mazhar
 *
 *         Feb 15, 2020
 */
public class ClientController implements MqttCallback {
	private ClientView view;
	private boolean isFanOn;
	private boolean isTvOn;
	private boolean isLightOn;
	MqttClient client = AtomClient.getConnection();

	public ClientController(ClientView view) {
		this.view = view;
		try {

			if (!client.isConnected()) {
				client.connect();
			}
			client.subscribe(FluidTopics.FAN_TOPICS);
			client.subscribe(FluidTopics.TV_TOPICS);
			client.subscribe(FluidTopics.LIGHT_TOPICS);
		} catch (MqttException e1) {
			e1.printStackTrace();
		}
		client.setCallback(this);
	}

	public void updateTvView() {

		if (isTvOn) {
			view.getMonitor().setIcon(new javax.swing.ImageIcon(getClass().getResource("/tv.gif")));
		} else {
			view.getMonitor().setIcon(new javax.swing.ImageIcon(getClass().getResource("/off.gif")));
		}

	}

	public void updateLightView() {

		if (isLightOn) {
			view.getMonitor().setIcon(new javax.swing.ImageIcon(getClass().getResource("/light220.gif")));
		} else {
			view.getMonitor().setIcon(new javax.swing.ImageIcon(getClass().getResource("/off.gif")));
		}

	}

	public void updateFanView() {

		if (isFanOn) {
			view.getMonitor().setIcon(new javax.swing.ImageIcon(getClass().getResource("/fan220.gif")));
		} else {
			view.getMonitor().setIcon(new javax.swing.ImageIcon(getClass().getResource("/off.gif")));
		}

	}

	@Override
	public void connectionLost(Throwable cause) {
		System.out.println("connection lost from client controller" + cause);

	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		new Thread(() -> {
			// System.out.println(message);
			customMessageParser(message);
		}).start();

	}

	/**
	 * @param message
	 */
	private void customMessageParser(MqttMessage message) {
		String data = message.toString();
		switch (data) {
		case FluidCommonMessages.COMMAND_FAN_ON:
			isFanOn = true;
			updateFanView();
			break;
		case FluidCommonMessages.COMMAND_FAN_OFF:
			isFanOn = false;
			updateFanView();
			break;
		case FluidCommonMessages.COMMAND_LIGHT_OFF:
			isLightOn = false;
			updateLightView();
			break;
		case FluidCommonMessages.COMMAND_LIGHT_ON:
			isLightOn = true;
			updateLightView();
			break;
		case FluidCommonMessages.COMMAND_TV_ON:
			isTvOn = true;
			updateTvView();
			break;
		case FluidCommonMessages.COMMAND_TV_OFF:
			isTvOn = false;
			updateTvView();
			break;
		}

	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		System.out.println("token delevery from client controller" + token);

	}

	private MqttMessage sendCommand(String message) {
		if (!StringUtils.nonEmptyString(message)) {
			message = FluidCommonMessages.NO_COMMAND;
		}
		MqttMessage mqMessage = new MqttMessage(message.getBytes());
		mqMessage.setQos(FluidDataUtil.PERSISTENCE_QOS);
		return mqMessage;
	}

}
