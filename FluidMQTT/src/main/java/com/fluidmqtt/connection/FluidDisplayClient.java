/*
 *singleton by nature and return connected client;
 * 
 */
package com.fluidmqtt.connection;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

/**
 * @author mazhar
 *
 *         Feb 13, 2020
 */
public class FluidDisplayClient {
	private static MqttClient client = null;
	private static FluidDisplayClient connection = new FluidDisplayClient();

	private FluidDisplayClient() {
	}

	public static MqttClient makeClient(String broker, String clientId) {
		MemoryPersistence persistence = new MemoryPersistence();
		if (client == null) {
			try {
				client = new MqttClient(broker, clientId, persistence);
			} catch (MqttException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return client;
	}

	public static MqttClient getConnection() {
		if (client == null) {
			return null;
		}
		if (!client.isConnected()) {
			try {
				client.connect();
			} catch (MqttSecurityException e) {
				e.printStackTrace();
			} catch (MqttException e) {
				e.printStackTrace();
			}
		}
		return client;
	}
}
