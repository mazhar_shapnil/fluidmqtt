/*
 *
 * every fluid object is a atom. so every fluid object receive message from atom client
 *and sent its state through Atom client
 *
 *singleton by nature and return non connected client
 * 
 */
package com.fluidmqtt.connection;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

/**
 * @author mazhar
 *
 *         Feb 18, 2020
 */
public class AtomClient {
	private static MqttClient client = null;
	private static AtomClient connection = new AtomClient();

	private AtomClient() {
	}

	public static MqttClient makeConnection(String broker, String clientId) {
		MemoryPersistence persistence = new MemoryPersistence();
		if (client == null) {
			try {
				client = new MqttClient(broker, clientId, persistence);
			} catch (MqttException e) {
				
				e.printStackTrace();
			}
		}
		return client;
	}

	public static MqttClient getConnection() {
		if (client == null) {
			return null;
		}
		return client;
	}
}
