/**
 * 
 */
package com.mqtt.mqttImplementation;

import com.fluidmqtt.connection.AtomClient;
import com.fluidmqtt.connection.FluidDisplayClient;
import com.fluidmqtt.ui.FluidWindow;
import com.fluidmqtt.util.FluidDataUtil;
/**
 * @author mazhar
 *
 * Feb 2, 2020
 */
public class UIApplication {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		FluidDisplayClient.makeClient(FluidDataUtil.PRIMARY_BROKER, FluidDataUtil.DISPLAY_CLIENT);
		AtomClient.makeConnection(FluidDataUtil.PRIMARY_BROKER, FluidDataUtil.ATOM_CLIENT);
		FluidWindow window = new FluidWindow();
        window.setVisible(true);
	}

}
